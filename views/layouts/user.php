<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\UserAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Category;

UserAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

	<script type="text/javascript" src="/public/js/jquery.js"></script>
	<script type="text/javascript" src="/public/js/plugins.js"></script>
	<script type="text/javascript" src="/public/js/main.js"></script>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="/public/plugins/html5.js"></script>
	<![endif]-->
</head>
<body class="sticky_footer">
<?php $this->beginBody() ?>

<div class="wrapper">

	<?= $this->render('/partials/header');?>

	<?= $content ?>

	<?= $this->render('/partials/footer');?>

</div>
<?php $this->endBody() ?>

<?php $this->registerJsFile('/ckeditor/ckeditor.js');?>
<?php $this->registerJsFile('/ckfinder/ckfinder.js');?>

<script>
	$(document).ready(function(){var editor = CKEDITOR.replaceAll(); CKFinder.setupCKEditor( editor );})
</script>

</body>
</html>
<?php $this->endPage() ?>
