<?php
use yii\helpers\Url;
?>
	<!-- HEADER BEGIN -->
<header>
		<div id="header">
			<section class="top">
				<div class="inner">
					<div class="block_top_menu">
						<nav>
							<ul>
								<li class=""><a href="<?= Url::toRoute(['/']) ?>">Главная</a></li>
								<li class=""><a href="<?= Url::toRoute(['/site/projects']) ?>">Проекты</a></li>
							</ul>
						</nav>
					</div>
					
					<div class="block_top_menu" style="float: right;">
						<nav>
							<ul>
								<?php if(Yii::$app->user->isGuest):?>
									<li><a href="<?= Url::toRoute(['/auth/login'])?>">Вход</a></li>
									<li><a href="<?= Url::toRoute(['/auth/signup'])?>">Регистрация</a></li>
								<?php else: ?>
									<?php if(Yii::$app->user->identity->isAdmin): ?>
										<li><a href="<?= Url::toRoute(['/admin'])?>">Админка</a></li>
									<?php endif ?>
									<li><a href="<?= Url::toRoute(['/user/my-projects'])?>">Мои проекты</a></li>
									<li><a href="<?= Url::toRoute(['/auth/logout'])?>">Выход <?= ' (' . Yii::$app->user->identity->name . ')' ?></a></li>
								<?php endif;?>
							</ul>
						</nav>
					</div>

					<div class="clearboth"></div>
				</div>
			</section>
			
			<section class="middle">
				<div class="inner">
					<!-- <div id="logo_top"><a href="index.html"><img src="images/logo_top.png" alt="Enterprise" title="Enterprise"></a></div> -->
					<center>
						<h1>KickBeginner</h1>
					</center>
				</div>
			</section>
			
		</div>
	</header>
	<!-- HEADER END -->