<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;

$this->title = 'Мои проекты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="block_general_title_1">
	<h1><?= $this->title ?></h1>
</div>
<div id="content" class="sidebar_right">
	<div class="inner">

		<p>
			<?= Html::a('Создать проект', ['create'], ['class' => 'btn btn-success']) ?>
		</p>
		<?= 
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [
					['class' => 'yii\grid\SerialColumn'],

					'id',
					'title',
					'start_date',
					'end_date',
					'need_sum',
					'collected_sum',
					// 'viewed',
					// 'user_id',
					// 'status',
					// 'category_id',

					['class' => 'yii\grid\ActionColumn'],
				],
			]); ?>

	</div>
</div>