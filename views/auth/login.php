<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="content" class="sidebar_right">
	<div class="inner">

		<div class="block_general_title_1 w_margin_1">
			<h1><?= $this->title ?></h1>
		</div>

		<div class="block_leave_comment_1 type_1">
			<div class="form">
				<?php 
					$form = ActiveForm::begin([
						'id' => 'login-form',
						'layout' => 'horizontal',
						'fieldConfig' => [
							'template' => "<div class=\"label\">{label}</div><div class=\"field\">{input}</div>{error}",
						],
					]);
				?>
					
					<?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
					
					<?= $form->field($model, 'password')->passwordInput() ?>
					
					<?= $form->field($model, 'rememberMe')->checkbox([
							'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
						])
					?>

					<div class="form-group">
						<div class="col-lg-offset-1 col-lg-11">
							<?= Html::submitButton('<p class="general_button_type_3">Отправить</p>', ['class' => 'btn-pay' ]) ?>
						</div>
					</div>

						<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>