<?php
use yii\helpers\Url;
?>

<header>
	<section class="bottom">
		<div class="inner">
			<div class="block_secondary_menu">
				<nav>
					<ul>
						<?php foreach($categories as $category):?>
							<li><a href="<?= Url::toRoute(['site/category','id'=>$category->id]);?>"><?= $category->title?></a></li>
						<?php endforeach;?>
					</ul>
				</nav>
			</div>
		</div>
	</section>
</header>

<!-- CONTENT BEGIN -->
<div id="content" class="sidebar_right">
	<div class="inner">
		<div class="block_general_title_2">
			<h1><?= $project->title ?></h1>
			<h2>
				<span class="date"><?= $project->getDate();?></span>
			</h2>
		</div>
		
		<div class="main_content">
			<div class="block_content">
				<div class="pic"><img src="<?= $project->getImage();?>" alt=""></div>
				
				<?= $project->content ?>

				<div class="line_1"></div>
			</div>
		</div>
		
		<?= $this->render('/partials/sidebar', [
			'project'=>$project,
			'payment'=>$payment,
			'popular'=>$popular,
			'recent'=>$recent,
			'categories'=>$categories,
			'is_single' => True
		]);?>
		
		<div class="clearboth"></div>
	</div>
</div>
<!-- CONTENT END -->
