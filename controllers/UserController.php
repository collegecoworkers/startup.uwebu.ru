<?php

namespace app\controllers;


use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

use app\models\Project;
use app\models\ProjectSearch;
use app\models\Category;
use app\models\LoginForm;
use app\models\Payment;
use app\models\ImageUpload;


class UserController extends Controller {
	
	public $layout = 'user';

	public function behaviors() {
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex() {
		return $this->redirect(['user/my-projects']);
	}

	public function actionMyProjects() {

		$searchModel = new ProjectSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

		return $this->render('projects', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	public function actionCreate() {
		$model = new Project();

		if ($model->load(Yii::$app->request->post()) && $model->saveProject()) {
			return $this->redirect(['view', 'id' => $model->id]);
			// return $this->redirect(['user/my-projects']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionUpdate($id) {
		$model = $this->findModel($id);
		
		if ($model->load(Yii::$app->request->post()) && $model->saveProject()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionSetImage($id) {
		$model = new ImageUpload;

		if (Yii::$app->request->isPost) {
			$article = $this->findModel($id);
			$file = UploadedFile::getInstance($model, 'image');

			if($article->saveImage($model->uploadFile($file, $article->image))) {
				return $this->redirect(['view', 'id'=>$article->id]);
			}
		}
		
		return $this->render('image', ['model'=>$model]);
	}
	
	public function actionSetCategory($id) {
		$article = $this->findModel($id);
		$selectedCategory = $article->category->id;
		$categories = ArrayHelper::map(Category::find()->all(), 'id', 'title');

		if(Yii::$app->request->isPost) {
			$category = Yii::$app->request->post('category');
			if($article->saveCategory($category)) {
				return $this->redirect(['view', 'id'=>$article->id]);
			}
		}

		return $this->render('category', [
			'article'=>$article,
			'selectedCategory'=>$selectedCategory,
			'categories'=>$categories
		]);
	}

	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	protected function findModel($id) {
		if (($model = Project::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
