<?php

namespace app\controllers;

use app\models\Project;
use app\models\Category;
// use app\models\CommentForm;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Payment;

class SiteController extends Controller {

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex() {
		$data = Project::getAll(6);
		$popular = Project::getPopular();
		$recent = Project::getRecent();
		$categories = Category::getAll();
		
		return $this->render('index',[
			'projects'=>$data['projects'],
			'pagination'=>$data['pagination'],
			'popular'=>$popular,
			'recent'=>$recent,
			'categories'=>$categories
		]);
	}

	public function actionProjects($id = null) {

		$data = ($id) ? Category::getProjectsByCategory($id) : Project::getAll(6);
		$categories = Category::getAll();
		
		return $this->render('projects',[
			'projects'=>$data['projects'],
			'pagination'=>$data['pagination'],
			'categories'=>$categories
		]);
	}

	public function actionView($id) {

		$project = Project::findOne($id);
		$popular = Project::getPopular();
		$recent = Project::getRecent();
		$categories = Category::getAll();
		$payment = new Payment();

		$project->viewedCounter();
		
		if(Yii::$app->request->isPost && !Yii::$app->user->isGuest){

			$payment->load(Yii::$app->request->post());

			$payment->user_id = Yii::$app->user->id;

			$payment->sum = Yii::$app->request->post()['Payment']['sum'];

			$payment->project_id = $project->id;
			$payment->date = date('Y-m-d');

			$payment->save();

			$project->recalc();

			return $this->redirect(['site/view','id'=>$id]);
		}

		return $this->render('single',[
			'project'=>$project,
			'popular'=>$popular,
			'recent'=>$recent,
			'categories'=>$categories,
			'payment'=>$payment,
		]);
	}

	public function actionCategory($id) {

		$data = Category::getProjectsByCategory($id);
		$popular = Project::getPopular();
		$recent = Project::getRecent();
		$categories = Category::getAll();
		
		return $this->render('category',[
			'projects'=>$data['projects'],
			'pagination'=>$data['pagination'],
			'popular'=>$popular,
			'recent'=>$recent,
			'categories'=>$categories
		]);
	}

}
