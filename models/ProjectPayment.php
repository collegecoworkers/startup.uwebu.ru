<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_payment".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $payment_id
 *
 * @property Payment $payment
 * @property Project $project
 */
class ProjectPayment extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'project_payment';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['project_id', 'payment_id'], 'integer'],
			[['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['payment_id' => 'id']],
			[['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'project_id' => 'Project ID',
			'payment_id' => 'Payment ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPayment()
	{
		return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProject()
	{
		return $this->hasOne(Project::className(), ['id' => 'project_id']);
	}
}
