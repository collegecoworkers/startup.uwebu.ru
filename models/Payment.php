<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property string $title
 *
 * @property ProjectPayment[] $projectPayments
 */
class Payment extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'payment';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['sum'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'user_id' => 'Пользоваетль',
			'project_id' => 'Проект',
			'sum' => 'Сумма',
			'date' => 'Дата',
		];
	}

	public function getProjects()
	{
		return $this->hasMany(Project::className(), ['id' => 'project_id'])
			->viaTable('project_payment', ['payment_id' => 'id']);
	}

	public function getByProject($project_id)
	{
		return Payment::find()->where(['project_id' => $project_id])->all();
	}
}
