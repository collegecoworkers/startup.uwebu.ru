<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment`.
 */
class m170124_021608_create_payment_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {
		$this->createTable('payment', [
			'id' => $this->primaryKey(),
			'user_id'=>$this->integer(),
			'project_id'=>$this->integer(),
			'sum'=>$this->integer(),
			'date'=>$this->date(),
		]);

		// creates index for column `user_id`
		$this->createIndex(
			'idx-post-user_id',
			'payment',
			'user_id'
		);

		// add foreign key for table `user`
		$this->addForeignKey(
			'fk-post-user_id',
			'payment',
			'user_id',
			'user',
			'id',
			'CASCADE'
		);


		// creates index for column `project_id`
		$this->createIndex(
			'idx-project_id',
			'payment',
			'project_id'
			);

		// add foreign key for table `project`
		$this->addForeignKey(
			'fk-project_id',
			'payment',
			'project_id',
			'project',
			'id',
			'CASCADE'
			);
	}

	/**
	 * @inheritdoc
	 */
	public function down() {
		$this->dropTable('payment');
	}
}
